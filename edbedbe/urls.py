from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'website.views.home', name='home'),
    url(r'fly$', 'website.views.fly', name='fly'),
    url(r'sealab$', 'website.views.sealab', name='sealab'),
    url(r'tradepaper$', 'website.views.tradepaper', name='tradepaper'),
    url(r'romance$', 'website.views.romance', name='romance'),
)
