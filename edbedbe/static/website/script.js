
// Begin Fontdeck

var html = document.getElementsByTagName('html')[0];
html.className += '  wf-loading';
setTimeout(function() {
  html.className = html.className.replace(' wf-loading', '');
}, 5000);

WebFontConfig = { fontdeck: { id: '54828' } };

(function() {
  var wf = document.createElement('script');
  wf.src = 'http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
  wf.type = 'text/javascript';
  wf.async = 'true';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(wf, s);
})();

// End Fontdeck


function showContact() {
  var contact = $('#contact-popup-container');
  contact.removeClass('hidden');
  contact.addClass('visible');
  contactIsVisible = true;
};

function hideContact() {
  var contact = $('#contact-popup-container');
  contact.removeClass('visible');
  contact.addClass('hidden');
  contactIsVisible = false;
};

$(document).ready(function() {

  var contactIsVisible = false;

  $('#contact').click(function () {
    showContact();
    return false;
  });

  $('#contact-popup-container').click(function () {
    hideContact();
    return false;
  }).children().click(function(e) {
  // This is so that you can click contact popup without popup disappearing
    e.stopPropagation();
  });

  $(document).keyup(function(e) {
    var KEYCODE_ESC = 27;
    if (e.keyCode == KEYCODE_ESC) {
      hideContact();
    };
  });

});
