import sys

from edbedbe.settings.common import *

DEBUG = False

TEMPLATE_DEBUG = False

# AWS_STORAGE_BUCKET_NAME = 'trade-paper'
#
# STATIC_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211'
    }
}

UPDATE_CACHE = ('django.middleware.cache.UpdateCacheMiddleware',)
FETCH_CACHE = ('django.middleware.cache.FetchFromCacheMiddleware',)

MIDDLEWARE_CLASSES = UPDATE_CACHE + MIDDLEWARE_CLASSES + FETCH_CACHE

CACHE_MIDDLEWARE_ALIAS = 'default'

CACHE_MIDDLEWARE_SECONDS = 600

CACHE_MIDDLEWARE_KEY_PREFIX = ''

# this is so for settings to use during tests
if 'test' in sys.argv:
    DATABASES['default'] = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'sealab'
    }

    STATIC_URL = '/static/'

    MEDIA_URL = '/media/'

    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'
