dfile = '/webapps/elibierman.com/tmp/gunicorn.pid'
bind = 'elibierman.com:8080'
workers = 3
accesslog = '/webapps/elibierman.com/logs/gunicorn-access.log'
errorlog = '/webapps/elibierman.com/logs/gunicorn-error.log'

