from fabric.api import lcd, local, hosts, run, cd, prefix, sudo, settings

@hosts('eli@eeman')
def deploy_prod():
    app_dir = '/webapps/elibierman.com'
    git_dir = '/webapps/elibierman.com/edbedbe'
    with cd(git_dir), prefix('export DJANGO_SETTINGS_MODULE=edbedbe.settings.production'):
        run('git pull origin master')
        run('pip install -r requirements.txt --allow-all-external')
        run('./manage.py migrate')
        run('./manage.py collectstatic')
        sudo('service nginx restart edbedbe')
    with cd(app_dir), prefix('export DJANGO_SETTINGS_MODULE=edbedbe.settings.production'):
        run('kill -HUP `cat tmp/gunicorn.pid`')

@hosts('eli@eeman')
def update_prod():
    print('\n\nMake sure you update your dev environment first!')
    app_dir = '/webapps/elibierman.com/'
    git_dir = '/webapps/elibierman.com/'
    with cd(git_dir):
        run('git pull origin master')
        run('pip install -r requirements.txt --allow-all-external')
    with cd(app_dir):
        sudo('apt-get update')
        sudo('apt-get upgrade')

