from django.shortcuts import render

def home(request):
    return render(request, 'website/home.html')

def fly(request):
    return render(request, 'website/fly.html')

def sealab(request):
    return render(request, 'website/sealab.html')

def romance(request):
    return render(request, 'website/romance.html')

def tradepaper(request):
    return render(request, 'website/trade-paper.html')

