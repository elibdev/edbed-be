#!/bin/sh

for f in scss/*.scss; do
  #isolate file name
  FILE="${f##*/}"
  FILE_NO_EXT="${FILE%.scss}"
  sass --compass "$f" "/Users/Ohio/projects/elibierman.com/edbedbe/edbedbe/static/website/${FILE_NO_EXT}.css"
  echo converted "$f" to "/Users/Ohio/projects/elibierman.com/edbedbe/edbedbe/static/website/${FILE_NO_EXT}.css"
done
